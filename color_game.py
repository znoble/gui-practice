from tkinter import *
from tkinter import ttk
import random

colors = ["red", "blue", "yellow", "purple", "orange", "black", "white", "green", "pink", "brown"]


def new_label(*args):
    text_value.set(random.choice(colors))
    lab.configure(foreground=random.choice(colors))


root = Tk()
root.title("Color Game")

mainframe = ttk.Frame(root, padding="12 12 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

text_value = StringVar()
text_value.set(random.choice(colors))
lab = ttk.Label(mainframe, textvariable=text_value, width=10, font=('Helvetica', 36), background="grey")
lab.grid(column=0, row=1, sticky=(W, E))
ttk.Button(mainframe, text="New Label", command=new_label).grid(column=1, row=1, sticky=(W, E))

for child in mainframe.winfo_children():
    child.grid_configure(padx=4, pady=4)

root.bind("<Return>", new_label)
root.mainloop()

